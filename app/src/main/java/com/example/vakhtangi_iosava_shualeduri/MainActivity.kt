package com.example.vakhtangi_iosava_shualeduri

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var cardNumber: TextView
    lateinit var name: TextView
    lateinit var date: TextView
    lateinit var ccv: TextView
    lateinit var cardDisplay: TextView
    lateinit var cardImage: ImageView
    lateinit var cardHolder: TextView
//    var today = Calendar.getInstance().getTime()
//    var formatter = SimpleDateFormat("MMyy", Locale.US)
//    var dates = formatter.format(today)
//    val sdf: SimpleDateFormat = SimpleDateFormat("MM/yy", Locale.US)
//    val cardDate = sdf.parse(date.text.toString())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cardNumber = findViewById(R.id.cardNum)
        name = findViewById(R.id.name_surname)
        date = findViewById(R.id.expire)
        ccv = findViewById(R.id.ccv)
        cardDisplay = findViewById(R.id.cardNumDisplay)
        cardHolder = findViewById(R.id.nameDisplay)
//        var exp = formatter.format(date.text.toString())
//        Log.d("ccc", dates)
//        Log.d("ccc", date.text.toString())
        cardDisplay.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (cardNumber.text.length == 16 && cardNumber.text.toString().startsWith("4")){
                    cardImage.setImageResource(R.drawable.visa)
                }
                else if (cardNumber.text.length == 16 && cardNumber.text.toString().startsWith("5")){
                    cardImage.setImageResource(R.drawable.mastercard)
                }
                if (name.text.isNotEmpty()){
                    cardHolder.setText(name.text)
                }
            }

            override fun afterTextChanged(p0: Editable?) {
                Log.d("cc", "ok")
            }

        })
    }

    fun save(view: View) {
        when {
            cardNumber.text.length != 16 -> {
                cardNumber.error = "wrong number"
            }
            cardNumber.text.toString().startsWith("4") -> {
                cardImage.setImageResource(R.drawable.visa)
            }
            cardNumber.text.toString().startsWith("5") -> {
                cardImage.setImageResource(R.drawable.mastercard)
            }
            name.text.isEmpty() -> {
                name.error = "name and surname must be filled"
            }
            ccv.text.length != 3 ->{
                ccv.error = "wrong ccv"
            }
//            SimpleDateFormat("MM/yy", Locale.US).parse(cardDate!!.toString())!!.before(Date()) ->{
//                date.error = "expired"
//            }
            else ->{
                Toast.makeText(this, "ბარათი წარატებით დაემატა", Toast.LENGTH_SHORT).show()
            }
        }
    }
}